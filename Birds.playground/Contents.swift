//: Playground - noun: a place where people can play

import UIKit

/*
 * This is the given protocol.
 * Task is: Write a class according to this protocol, you are free to make output in any way you want in cases where output is needed
 */


protocol Bird {
    func setKind(type: String)
    func getKind()
    
    func getLocation()
    func setLocation(location: Location)
    func flyTo(location: Location)
    
    func setMelody(melody: String)
    
    func singMelody() // output depends on a kind
    
    func killBird()
    func cloneBird(count: Int)
}

//The location class
class Location: NSObject {
    var x:Int
    var y:Int
    var z:Int
    
    init(x:Int, y:Int, z:Int){
        self.x = x
        self.y = y
        self.z = z
    }
}

class Flock: Bird {
    
    var type: String?
    var location: Location?
    var melody: String?
    
    //for this implementation, we will default to 1 bird
    var count: Int = 1
    
    func setKind(type: String) {
        self.type = type
    }
    
    func getKind() {
        if let kind = type {
            print("It is a type of \(kind).")
        }
    }
    
    func getLocation() {
        if let loc = location {
            print("It's location is on coordinates: x = \(loc.x), y = \(loc.y), z = \(loc.z)")
        }else{
            print("It has no location yet. It maybe on point zero (or in the twilight zone)...")
        }
    }
    
    func setLocation(location: Location) {
        self.location = location
    }
    
    func flyTo(location: Location) {
        if let oldloc = self.location {
            print("Before flying, it is on coordinates: x = \(oldloc.x), y = \(oldloc.y), z = \(oldloc.z)")
        }
        
        self.location = location
        
        if let newloc = self.location {
            print("It flew to the new location with coordinates: x = \(newloc.x), y = \(newloc.y), z = \(newloc.z)")
        }
    }
    
    func setMelody(melody: String) {
        self.melody = melody
    }
    
    func singMelody() {
        if let kind = type, let sound = melody {
            print("\(kind) bird sings \(sound) ♩ ♪ ♫ ♬")
        }
    }
    
    func killBird() {
        if count == 0 {
            print("There are no more birds to kill...")
        }else {
            count -= 1
            print("One bird was killed off...")
        }
    }
    
    func cloneBird(count: Int) {
        print("Bird count before cloning = \(self.count)")
        
        self.count += count
        print("Bird count after cloning = \(self.count)")
    }
    
}

//sample usage for the class
var tweety = Flock()
tweety.setKind(type: "Canary")
tweety.getKind()
tweety.getLocation()
tweety.setLocation(location: Location(x: 10, y: 40, z: 100))
tweety.getLocation()
tweety.flyTo(location: Location(x: 300, y: 90, z: 50))
tweety.setMelody(melody: "do dee do dee do")
tweety.singMelody()
tweety.killBird()
tweety.cloneBird(count: 5)


