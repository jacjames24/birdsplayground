#BirdsPlayground README

- this playground files contains a set of codes that delves into how protocols and the classes that conforms to them work.
- this playground class is written in **Swift 3.0**
- in case you would like to view the contents of the classes and protocol, you can directly view them here: [Contents.swift](https://bitbucket.org/jacjames24/birdsplayground/src/7a63d8e974b8618e4d28510d381a12699df118a2/Birds.playground/Contents.swift?at=master)